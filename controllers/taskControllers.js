// Contains all the functions and business logics of our application
const Task = require("../models/Task.js");

module.exports.getAllTasks = () => {

	/*
	return Task.find({}) =
	[{
		_id
		64257cc08ae8f581b00633a9
		name
		"Eat Dinner"
		status
		"pending"
		__v
		0
	}]

	*/

	return Task.find({}).then(result => {

		return result;
	})
}

module.exports.createTask = (requestBody) => {

	return Task.findOne({name: requestBody.name}).then((result, error) => {

		if(result != null && result.name == requestBody.name){

			return `Duplicate Task Found`;
		} else {
			let newTask = new Task ({
				name: requestBody.name 
			});

			return newTask.save().then((savedTask, savedError) => {

				if (savedError) {

					console.lof(savedError)
					return 'Task Creation Failed!';

				} else {

					return savedTask;
				}
			})
		}
	})
}

module.exports.deleteTask = (paramsId) => {

	return Task.findByIdAndRemove(paramsId).then((removedTask, err) => {

		if(err) {

			console.log(err);
			return 'Task was not Removed :( '
		} else {

			return 'Task Removed Successfully! :) ! '
		}
	})
};

module.exports.updateTask = (paramsId, requestBody) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// "findById" is the same as "find({"_id" : value})"
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(paramsId).then((result, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err) {

			console.log(error);
			return 'Error Found';

		} else {

			// Results of the "findById" method will be stored in the "result" parameter
			// It's "name" property will be reassigned the value of the "name" received from the request
			result.name = requestBody.name

			
			// Saves the updated object in the MongoDB database
			// The document already exists in the database and was stored in the "result" parameter
			// Because of Mongoose we have access to the "save" method to update the existing document with the changes we applied
			// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method which invokes this function
			return result.save().then((updatedTask, err) => {

				// If an error is encountered returns a "false" boolean back to the client/Postman
				if(err) {
					
					// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
					console.log(err);
					return false;
				
				// Update successful, returns the updated task object back to the client/Postman
				} else {
					
					// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
					return updatedTask;
				}
			})
		}
	})
};

module.exports.getTask = (paramsId, requestBody) => {

		return Task.findById(paramsId).then(result => {

		return result;
	})
};



module.exports.completeTask = (paramsId, requestBody) => {


	return Task.findById(paramsId).then((result, err) => {

		if(err) {
			console.log(error);
			return 'Error Found';
		} else {
			result.status = requestBody.status

			return result.save().then((updatedStatusTask, err) => {
				if(err) {
					
					console.log(err);
					return false;

				} else {
					
					
					return updatedStatusTask;
				}
			})
		}
	})
};



