// Contains all the URI endpoints for our application
const express = require("express");
// Creates a Router instance that functions as middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();
const taskController = require("../controllers/taskControllers.js");

// [Section] Routes 
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller
router.get("/", (req, res) => {

	/*
	return Task.find({}).then(result => {

		return result;
	})

	getAllTask = [{
		_id
		64257cc08ae8f581b00633a9
		name
		"Eat Dinner"
		status
		"pending"
		__v
		0
	}]

	resultFromController = [{
		_id
		64257cc08ae8f581b00633a9
		name
		"Eat Dinner"
		status
		"pending"
		__v
		0
	}]
	*/

	taskController.getAllTasks().then(resultFromController => res.send({resultFromController}))
});

router.post("/create", (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send({resultFromController}))
});


router.delete("/:id", (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send({resultFromController}))
});

router.put("/:id", (req, res) => {

	// The "updateTask" function will accept the following 2 arguments:
		// "req.params.id" retrieves the task ID from the parameter
		// "req.body" retrieves the data of the updates that will be applied to a task from the request's "body" property
	taskController.updateTask(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
})


router.get("/:id", (req,res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send({resultFromController}))
});



router.put("/:id/complete", (req, res) => {
	taskController.completeTask(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
})

module.exports = router;